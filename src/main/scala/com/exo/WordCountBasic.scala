package com.exo

import com.exo.util.RegexUtils

trait WordCountBasic extends WordCount {

  def doWordcount(text: String): Map[String, Int] = {
    println(s"INFO: Doing the counting...")
    val wordAndOneTuples: Array[(String, Int)] = text.split(RegexUtils.regexSentenceEndLineBreak).flatMap { sentence =>
      sentence
        .replaceAll(RegexUtils.regexPunct, "") // Removing punctuation
        .split(RegexUtils.regexSpaces) // Splitting sentence into words
        .filterNot(_.isBlank) // Remove empty and white spaces strings
        .map(word => (word.toLowerCase, 1))
    }
    val wordcount: Map[String, Int] = wordAndOneTuples
      .groupBy(x => x._1)
      .map(x => x._1 -> x._2.map(_._2).reduce((x, y) => x + y))
    wordcount
  }
}
