package com.exo

import com.exo.util.RegexUtils

import scala.collection.parallel.CollectionConverters._
import scala.util.Random

trait WordCountParallel extends WordCount {

  val nbPartitions: Int = 4

  def doWordcount(text: String): Map[String, Int] = {
    println(s"INFO: Doing the counting...")
    val partitions = text
      .split(RegexUtils.regexSentenceEndLineBreak)
      .groupMap(_ => Random.between(0, nbPartitions))(x => x)
      .values.toList.par
    val wordAndOneTuples = partitions map { sentencesBatch =>
      sentencesBatch map { sentence =>
        sentence
          .replaceAll(RegexUtils.regexPunct, "") // Removing punctuation
          .split(RegexUtils.regexSpaces) // Splitting sentence into words
          .filterNot(_.isBlank) // Remove empty and white spaces strings
          .map(word => (word.toLowerCase, 1))
      }
    }
    val wordcount = wordAndOneTuples.seq.flatten.flatten
      .groupBy(x => x._1)
      .map(x => x._1 -> x._2.map(_._2).reduce((x, y) => x + y))
    wordcount
  }
}
